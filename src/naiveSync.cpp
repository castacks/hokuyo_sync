// See http://www.ros.org/wiki/message_filters/ApproximateTime
#include <ros/ros.h>
#include <sensor_msgs/LaserScan.h>
#include <nav_msgs/Odometry.h>


ros::Time odomTime;
ros::Publisher sync_scan_pub;
double scanOffset;

void scanCallback(const sensor_msgs::LaserScan::ConstPtr& scan)
{

    // Create synchronized version of the scan
    sensor_msgs::LaserScan sync_scan = *scan;
    sync_scan.header.stamp       = odomTime + ros::Duration(0.003125); // FIX magic number: It is the time for the laser to go 45 degrees spinning at 40Hz
    sync_scan.header.stamp  = sync_scan.header.stamp + ros::Duration(scanOffset / 40);
    sync_scan_pub.publish(sync_scan);
}


void odomCallback(const nav_msgs::Odometry::ConstPtr& odomMsg)
{
  odomTime = odomMsg->header.stamp;

}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "laser_pps_sync_node");
  ros::NodeHandle n;
  ros::NodeHandle np = ros::NodeHandle("~");
  ros::Subscriber sub_odom = n.subscribe("/encoderOdom", 10, odomCallback);
  ros::Subscriber sub_scan = n.subscribe("/scan", 10, scanCallback);

  sync_scan_pub = n.advertise<sensor_msgs::LaserScan>("/sync_scan", 10);

	np.param<double>("scanOffset", scanOffset, 0);

  ros::spin();

  return 0;
}
