// See http://www.ros.org/wiki/message_filters/ApproximateTime
#include <ros/ros.h>
#include <sensor_msgs/LaserScan.h>
#include <nav_msgs/Odometry.h>
#include <queue>

ros::Time odomTime;
ros::Publisher sync_scan_pub;
double scanOffset;
unsigned int prevOdomSeq = 0;
unsigned int prevLaserSeq = 0;


std::queue<sensor_msgs::LaserScan::ConstPtr> laserQueue;
std::queue<nav_msgs::Odometry::ConstPtr> odomQueue;

void scanCallback(const sensor_msgs::LaserScan::ConstPtr& scan);
void odomCallback(const nav_msgs::Odometry::ConstPtr& odomMsg);
void checkQueues(void);

void scanCallback(const sensor_msgs::LaserScan::ConstPtr& scan)
{
	laserQueue.push(scan);
	checkQueues();
}


void odomCallback(const nav_msgs::Odometry::ConstPtr& odomMsg)
{
	odomQueue.push(odomMsg);
	checkQueues();
}

void checkQueues(void)
{
	while (!odomQueue.empty() && !laserQueue.empty())
	{
		
		if (prevOdomSeq == 0)
			prevOdomSeq = odomQueue.front()->header.seq - 1;
			
		if (prevLaserSeq == 0)
			prevLaserSeq = laserQueue.front()->header.seq - 1;
			
			
		//ROS_INFO_STREAM(laserQueue.front()->header.seq << " " << odomQueue.front()->header.seq);
		
		int seqDiff;
		
		do
		{
			seqDiff = (laserQueue.front()->header.seq - prevLaserSeq) - (odomQueue.front()->header.seq - prevOdomSeq);
			
			if (seqDiff > 0)
			{
				ROS_ERROR("hokuyo_sync: Throwing out odometry msg");
				ROS_INFO_STREAM(laserQueue.front()->header.seq << " " << odomQueue.front()->header.seq);
				odomQueue.pop();
			}
			else if (seqDiff < 0)
			{
				ROS_ERROR("hokuyo_sync: Throwing out laser msg");
				ROS_INFO_STREAM(laserQueue.front()->header.seq << " " << odomQueue.front()->header.seq);
				laserQueue.pop();
			}
		
		} while (seqDiff != 0 && !odomQueue.empty() && !laserQueue.empty());
		
		if (odomQueue.empty() || laserQueue.empty())
		{
			ROS_ERROR("Empty sync queue");
			break;
		}
	// Create synchronized version of the scan
    sensor_msgs::LaserScan sync_scan = *(laserQueue.front());
    sync_scan.header.stamp       = odomQueue.front()->header.stamp + ros::Duration(0.003125); // Magic number: It is the time for the laser to go 45 degrees from the sync pulse location and reach the first scan point when spinning at 40Hz
    sync_scan_pub.publish(sync_scan);
    
    prevOdomSeq = odomQueue.front()->header.seq;
    prevLaserSeq = laserQueue.front()->header.seq;
    
    laserQueue.pop();
    odomQueue.pop();
	
	}
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "laser_pps_sync_node");
  ros::NodeHandle n;
  ros::NodeHandle np = ros::NodeHandle("~");
  ros::Subscriber sub_odom = n.subscribe("/encoderOdom", 10, odomCallback);
  ros::Subscriber sub_scan = n.subscribe("/scan", 10, scanCallback);

  sync_scan_pub = n.advertise<sensor_msgs::LaserScan>("/sync_scan", 10);

	np.param<double>("scanOffset", scanOffset, 0);

  ros::spin();

  return 0;
}
