// See http://www.ros.org/wiki/message_filters/ApproximateTime
#include <ros/ros.h>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/LaserScan.h>
#include <nav_msgs/Odometry.h>

//using namespace sensor_msgs;
using namespace message_filters;
//using namespace nav_msgs;

class PPSync {

public:
  ros::NodeHandle nh_, np_;
  ros::Publisher scan_pub_;
  message_filters::Subscriber<sensor_msgs::LaserScan> scan_sub_;
  message_filters::Subscriber<nav_msgs::Odometry> odom_sub_;
  typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::LaserScan, nav_msgs::Odometry> MySyncPolicy;
  message_filters::Synchronizer<MySyncPolicy> sync_;
  ros::Publisher sync_scan_pub_;
	double scanOffset;

  PPSync(ros::NodeHandle nh, ros::NodeHandle np):
    nh_(nh),
    scan_sub_(nh_, "/scan", 10),
    odom_sub_(nh_, "/encoderOdom", 10),
    sync_(MySyncPolicy(10), scan_sub_, odom_sub_),    // ApproximateTime takes a queue size as its constructor argument, hence MySyncPolicy(10)
    np_(np)
  {

		double maxIntervalDuration;

		if (np_.getParam("maxIntervalDuration", maxIntervalDuration))
		{
			ROS_INFO_STREAM("hokuyo_sync: setMaxIntervalDuration to " << maxIntervalDuration );
			sync_.setMaxIntervalDuration(ros::Duration(maxIntervalDuration));
		}
		else
			ROS_INFO("hokuyo_sync: maxIntervalDuration set to default");

    sync_scan_pub_ = nh_.advertise<sensor_msgs::LaserScan>("/sync_scan", 1);
    sync_.registerCallback(boost::bind(&PPSync::callback, this, _1, _2));

		np_.param<double>("scanOffset", scanOffset, 0);
  }
  
  void callback(const sensor_msgs::LaserScan::ConstPtr& scan, const nav_msgs::Odometry::ConstPtr& odom)
  {
	  np_.getParamCached("scanOffset", scanOffset);
    // Create synchronized version of the scan
    sensor_msgs::LaserScan sync_scan = *scan;
    sync_scan.header.stamp       = odom->header.stamp + ros::Duration(0.003125); // FIX magic number: It is the time for the laser to go 45 degrees spinning at 40Hz
		sync_scan.header.stamp  = sync_scan.header.stamp + ros::Duration(scanOffset / 40);
    sync_scan_pub_.publish(sync_scan);
  }
  
}; // end PPsync class


int main(int argc, char** argv)
{
  ros::init(argc, argv, "laser_pps_sync_node");
  
  ros::NodeHandle nh;
  		ros::NodeHandle np("~");
  PPSync sync_object(nh, np);
  ros::spin();

  return 0;
}
